const webpack = require("webpack");
const NodemonPlugin = require('nodemon-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const nodeExternals = require('webpack-node-externals');

const path = require('path');

module.exports = {
    mode: 'development',
    entry: './index',
    devtool: 'source-map',
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'dist'),
        hotUpdateChunkFilename: 'hot/hot-update.js',
        hotUpdateMainFilename: 'hot/hot-update.json'
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /^node_modules/,
                use: [{
                    loader: 'ts-loader',
                    options: {
                        transpileOnly: true
                    }
                }]
            }
        ]
    },
    devServer: {
        historyApiFallback: true
    },
    resolve: {
        extensions: [".ts"],
        plugins: [
            new TsconfigPathsPlugin({
                configFile: "./tsconfig.json",
                logLevel: "info",
                extensions: [".ts", ".tsx"],
                mainFields: ["browser", "index"],
            })
        ]
    },
    node: {
        __filename: false,
        __dirname: false,
    },
    context: path.resolve(__dirname, 'src'),
    target: "node",
    externals: [
        nodeExternals({
            allowlist: ['webpack/hot/poll?100'],
        }),
    ],
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new NodemonPlugin()
    ]
};
