import * as moment from 'moment';
import { Logger } from 'winston';

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf, label } = format;

const myFormat = printf(info => {
    if (typeof info.message === 'string') {
        if (info.stack) {
            return `${process.pid} ${info.timestamp} [${info.label}] ${info.level}: ${info.message} ${JSON.stringify(info.stack)}`;
        } else {
            return `${process.pid} ${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
        }
    } else if (typeof info.message === 'object') {
        return `${process.pid} ${info.timestamp} [${info.label}] ${info.level}: ${JSON.stringify(info.message)}`;
    }
});

const logger: Logger = createLogger({
    level: 'debug',
    timestamp: () => {
        return moment().format('DD.MM.YYYY HH:mm:ss');
    },
    format: combine(
        timestamp({ format: 'DD.MM.YYYY HH:mm:ss.ms' }),
        label({ label: 'DEFI' }),
        myFormat
    ),

    transports: [
        new transports.Console({ level: 'debug' })
    ]
});
export { logger };
