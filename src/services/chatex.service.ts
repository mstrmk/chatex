import { User } from '../database/models/user';
import { Token } from '../database/models/token';
import { Tournaments } from '../database/models/tournaments';
import { Bets } from '../database/models/bets';

const axios = require('axios');

// https://api.chatex.com/v1/auth/access-token

export class ChatexService {

    private readonly DOMAIN = 'https://api.staging.iserverbot.ru/v1/';
    private readonly chatexToken = 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJkZmQ3YmU2OC1hNzFjLTQyYTItYTU1My00NmFhZTBlMzg4Y2YiLCJpYXQiOjE2MzI1Nzc4NjQsImlzcyI6ImNoYXRleC1pZCIsInN1YiI6InVzZXIiLCJ1aWQiOjE0NTAsInZlciI6MSwicmVzIjpbMV0sInR5cCI6MSwic2NvcGVzIjpbImNvcmU6cmVhZCIsInByb2ZpbGU6cmVhZCIsInByb2ZpbGU6YWN0aW9uIiwiYXV0aDphY3Rpb24iLCJjaGF0ZXhfcGF5OnJlYWQiLCJjaGF0ZXhfcGF5OmFjdGlvbiIsImV4Y2hhbmdlOnJlYWQiLCJleGNoYW5nZTphY3Rpb24iLCJub3RpZmljYXRpb25zOnJlYWQiLCJub3RpZmljYXRpb25zOmFjdGlvbiIsIndhbGxldDpyZWFkIiwid2FsbGV0OmFjdGlvbiIsImFmZmlsaWF0ZTpyZWFkIiwiYWZmaWxpYXRlOmFjdGlvbiIsImNvbnZlcnNhdGlvbjpyZWFkIiwiY29udmVyc2F0aW9uOmFjdGlvbiIsInBheW91dHM6cmVhZCIsInBheW91dHM6YWN0aW9uIl0sImlzXzJmYV9kaXNhYmxlZCI6ZmFsc2V9.-NsBuh4NkW0u-o3FOxV-4Wg4J-zcyAZg7Z6InaxsiLe4oCACDdu87tizDIR_0vq6g4x5JIBWNVhai8S8cRlocQ';
    private sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

    public test = async () => {
        try {
            const response = await axios.post(this.DOMAIN + 'auth/access-token', {}, {
                headers: {
                    'Authorization': `Bearer ${this.chatexToken}`
                }
            });
            console.log(response.data);
        } catch (error) {
            console.error(error.response);
        }
    };

    public auth = async (discordId: string, login: string) => {
        try {
            const response = await axios.post(this.DOMAIN + 'auth', {
                mode: 'CHATEX_BOT',
                identification: login
            }, {});

            console.log(response.data);

            const user = await User.findOne({ where: { discordId } });

            if (user) {
                user.wallet = login;

                await user.save();

                while (true) {
                    await this.sleep(3000);

                    const data = await this.check(response.data.state.request_id);

                    if (data.status === 'APPROVED') {
                        await Token.delete({ user: { id: user.id } });

                        await Token.create({ user, refresh: data.refresh_token } as Token).save();
                        break;
                    } else if (data.status === 'EXPIRED') {
                        return { message: 'Повторите попытку' };
                    }

                }
            }


        } catch (error) {
            console.error(error.response);
        }
    };

    public check = async (requestId: string) => {
        try {
            const response = await axios.post(this.DOMAIN + 'auth/wait-confirmation', {
                request_id: requestId
            }, {});

            console.log(response.data);

            return response.data;

        } catch (error) {
            console.error(error.response);
        }
    };

    public getToken = async (userId: number) => {
        try {
            const token = await Token.findOne({ where: { user: { id: userId } } });

            if (token) {
                const response = await axios.post(this.DOMAIN + 'auth/access-token', {}, {
                    headers: {
                        'Authorization': `Bearer ${token.refresh}`
                    }
                });
                console.log('tokenGet', response);

                token.expires = response.data.expires_at;
                token.token = response.data.access_token;

                await token.save();

                return token;
            }

            return;
        } catch (error) {
            console.error(error);
        }
    };

    public transfer = async (userId: number, code, tournament: Tournaments) => {
        try {
            const token = await Token.findOne({ where: { user: { id: userId } } });

            if (token) {
                const response = await axios.post(this.DOMAIN + 'wallet/transfers', {
                    coin: 'btc',
                    amount: tournament.bet,
                    recipient: 'kotel',
                    second_factor: {
                        mode: 'PIN',
                        code
                    }
                }, {
                    headers: {
                        'Authorization': `Bearer ${token.token}`
                    }
                });

                console.log(response.data);

                await Bets.create({
                    amount: tournament.bet,
                    user: { id: userId },
                    status: 'PAID',
                    tournament: { id: tournament.id },
                } as Bets).save();

                return true;
            }

            return false;
        } catch (error) {
            console.error(error.response);
            return false;
        }
    };

}


