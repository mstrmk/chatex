import { Tournaments } from '../database/models/tournaments';
import { User } from '../database/models/user';
import { Team } from '../database/models/team';
import { TeamUser } from '../database/models/team.user';
const crypto = require('crypto');

export class TeamService {

    public create = async (user: User, tournament: Tournaments, name: string, users: any[]) => {

        const created = await Team.create({
            createdBy: user,
            tournament: tournament,
            name: name
        } as Team).save();

        created.hash = crypto.createHash('md5').update(`${name}_${created.id}`).digest('hex');

        await created.save();

        if (users.length) {
            try {
                await TeamUser.save(users.map(u => {
                    return TeamUser.create({
                        team: { id: created.id },
                        steamId: u
                    } as TeamUser);
                }));
            } catch (e) {
                console.log(e);
            }
        }


        return created;
    };

}
