import { User } from '../database/models/user';

export class UserService {

    public check = (discordId: string): Promise<User> => {
        return User.findOne({ where: { discordId: discordId } });
    };

    public registration = async (user): Promise<User> => {

        const exist = await this.check(user.id);

        console.log(exist);
        if (exist) {
            return exist;
        }

        return await User.create({
            discordId: user.id,
            name: `${user.username}#${user.discriminator}`
        } as User).save();
    };

    public get = async (profile) => {
        let user = await this.check(profile.id);
        if (!user) {
            user = await this.registration(profile);
        }
        return user;
    }

}


