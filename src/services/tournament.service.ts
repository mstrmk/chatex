import { Tournaments } from '../database/models/tournaments';
import { MoreThanOrEqual } from 'typeorm';
import * as moment from 'moment';
import { User } from '../database/models/user';
import { TournamentTypeEnum } from '../database/models/enums/tournament.type.enum';

export class TournamentService {

    public find = async (): Promise<Tournaments[]> => {
        const tournaments = await Tournaments.find({
            where: {
                start: MoreThanOrEqual(moment().toDate())
            },
            relations: ['createdBy']
        });

        return tournaments.map(tournament => {
            tournament.text = `${tournament.name} - ${tournament.countTeams} команд(ы) - взнос ${tournament.bet} BTC - `
                + `старт ${moment(tournament.start).format('DD.MM.YYYY HH:mm')} - конец регистрации ${moment(tournament.endOfRegistration).format('DD.MM.YYYY HH:mm')}`;

            return tournament;
        });
    };

    public create = async (
        name: string,
        user: User,
        start: Date,
        endOfRegistration: Date,
        countTeams: number,
        bet: number
    ): Promise<Tournaments> => {

        let type: TournamentTypeEnum;

        if (countTeams <= 4) {
            type = TournamentTypeEnum.ONLY_ONE;
        } else if (countTeams <= 8) {
            type = TournamentTypeEnum.TOP_THREE_50_30_20;
        } else {
            type = TournamentTypeEnum.TOP_THREE_60_30_10;
        }

        return await Tournaments.create({
            name,
            createdBy: user,
            start,
            endOfRegistration,
            bet,
            countTeams,
            type
        } as Tournaments).save();

    };

}


