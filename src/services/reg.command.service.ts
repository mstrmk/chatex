const { SlashCommandBuilder } = require('@discordjs/builders');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const { clientId, guildId, token } = require('../../config.json');

export class RegCommandService {

    private readonly commands = [
        new SlashCommandBuilder().setName('ping').setDescription('Replies with pong!'),
        new SlashCommandBuilder().setName('start').setDescription('Start or restart direct messages script with bot!'),
        new SlashCommandBuilder().setName('info').setDescription('Replies with info about us!'),
    ]
        .map(command => command.toJSON());

    private readonly rest = new REST({ version: '9' }).setToken(token);

    public registration = async () => {

        // await this.rest.put(Routes.applicationCommands(clientId), { body: this.commands });
        // await this.rest.put(Routes.applicationCommand(clientId, guildId), { body: this.commands });

        await this.rest.put(Routes.applicationGuildCommands(clientId, guildId), { body: this.commands });

        console.log('Successfully registered application commands.')

    };

}





