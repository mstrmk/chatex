

const { MessageActionRow, MessageButton, MessageSelectMenu } = require('discord.js');

export class BotService {

    public makeRow() {
        return new MessageActionRow();
    }

    public makeButton(customId, label, style = 'PRIMARY') {
        return new MessageButton()
        .setCustomId(customId)
        .setLabel(label)
        .setStyle(style);
    }

    public makeSelectMenu(customId, items, placeholder = 'Выберите') {
        return new MessageSelectMenu()
					.setCustomId(customId)
					.setPlaceholder(placeholder)
					.addOptions(items);
    }

}