import { QueryLogger } from '@config/./querry.logger';
import 'reflect-metadata';
import { createConnection } from 'typeorm';
import { logger } from '@config/logger';
import { User } from './models/user';
import { Tournaments } from './models/tournaments';
import { Matches } from './models/matches';
import { Bets } from './models/bets';
import { Team } from './models/team';
import { TeamUser } from './models/team.user';
import { Token } from './models/token';

const config = require('../../config.json');

class PostgreSQL {
    private static options: any = {
        logging: true,
        logger: new QueryLogger(),
        maxQueryExecutionTime: 60000,
        type: 'postgres',
        entities: [
            User,
            Tournaments,
            Matches,
            Bets,
            Team,
            TeamUser,
            Token
        ],
        synchronize: false,
    };

    public static async createConnection() {

        PostgreSQL.options.port = config.database.defi.port;
        PostgreSQL.options.username = config.database.defi.userName;
        PostgreSQL.options.password = config.database.defi.password;
        PostgreSQL.options.database = config.database.defi.name;

        if (process.env.ENVIROMENT !== 'production') {
            PostgreSQL.options.host = config.database.defi.host;
        } else if (process.env.ENVIROMENT === 'production') {
            PostgreSQL.options.host = 'localhost';
        }

        try {
            const connection = await createConnection(PostgreSQL.options);

            logger.info(
                'Connected to PostgreSQL: pgsql://' + PostgreSQL.options.host + ':' + PostgreSQL.options.port + '/' + PostgreSQL.options.database,
            );

            return connection;
        } catch (e) {
            logger.error(e);
        }
    }
}

export { PostgreSQL };
