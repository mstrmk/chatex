import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn, } from 'typeorm';
import { Tournaments } from './tournaments';
import { Team } from './team';
import { Token } from './token';

@Entity('user', {
    schema: 'chatex',
})

export class User extends BaseEntity {

    @PrimaryGeneratedColumn() id: number;

    @Column('timestamp without time zone', {
        nullable: true,
        default: () => '(now() at time zone \'Europe/Moscow\')',
    })
    created: Date;

    @Column('character varying', {
        nullable: true,
        length: 128,
        name: 'name'
    })
    name: string;

    @Column('character varying', {
        name: 'discord_id'
    })
    discordId: string;

    @OneToMany(() => Tournaments, tournament => tournament.createdBy)
    tournaments: Tournaments[];

    @OneToMany(() => Team, teams => teams.createdBy)
    teams: Team[];

    @OneToMany(() => Token, token => token.user)
    tokens: Token[];

    @Column('json', {
        nullable: true
    })
    state: any;

    @Column('character varying', {
        nullable: true,
        name: 'state_status'
    })
    status: string;

    @Column('character varying', {
        nullable: true,
        name: 'wallet'
    })
    wallet: string;

}
