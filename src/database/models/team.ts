import { BaseEntity, Column, Entity, JoinColumn, ManyToMany, ManyToOne, PrimaryGeneratedColumn, } from 'typeorm';
import { User } from "./user";
import { Tournaments } from "./tournaments";
import { TeamUser } from "./team.user";


@Entity('team', {
    schema: 'chatex',
})

export class Team extends BaseEntity {

    @PrimaryGeneratedColumn() id: number;

    @Column('timestamp without time zone', {
        nullable: true,
        default: () => '(now() at time zone \'Europe/Moscow\')',
    })
    created: Date;

    @Column('character varying', {
        nullable: true,
        length: 128,
        name: 'name'
    })
    name: string;

    @Column('character varying', {
        nullable: true,
        length: 128,
        name: 'hash'
    })
    hash: string;

    @ManyToOne(() => User, user => user.teams)
    @JoinColumn({
        name: 'created_by_id',
        referencedColumnName: 'id'
    })
    createdBy: User;

    @ManyToOne(() => Tournaments, tournament => tournament.teams)
    @JoinColumn({
        name: 'tournament_id',
        referencedColumnName: 'id'
    })
    tournament: Tournaments;

    @ManyToMany(() => TeamUser, teamUsers => teamUsers.team)
    users: User[];
}
