import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, } from 'typeorm';
import { Team } from "./team";
import { User } from "./user";

@Entity('team_user', {
    schema: 'chatex',
})

export class TeamUser extends BaseEntity {

    @PrimaryGeneratedColumn() id: number;

    @ManyToOne(() => Team, team => team.users)
    @JoinColumn({
        name: 'team_id',
        referencedColumnName: 'id'
    })
    team: Team;

    @Column('character varying', {
        name: 'steam_id'
    })
    steamId: string;

    @ManyToOne(() => User)
    @JoinColumn({
        name: 'user_id',
        referencedColumnName: 'id'
    })
    user: User;
}
