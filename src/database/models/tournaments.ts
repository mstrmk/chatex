import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, } from 'typeorm';
import { User } from './user';
import { TournamentTypeEnum } from './enums/tournament.type.enum';
import { Team } from './team';
import { Bets } from './bets';
import { Matches } from './matches';


@Entity('tournaments', {
    schema: 'chatex',
})

export class Tournaments extends BaseEntity {

    @PrimaryGeneratedColumn() id: number;

    @Column('timestamp without time zone', {
        nullable: true,
        default: () => '(now() at time zone \'Europe/Moscow\')',
    })
    created: Date;

    @Column('character varying', {
        nullable: true,
        length: 128,
        name: 'name'
    })
    name: string;

    text?: string;

    @Column('timestamp without time zone', {
        nullable: true,
    })
    start: Date;

    @Column('timestamp without time zone', {
        nullable: true,
        name: 'deadline'
    })
    endOfRegistration: Date;

    @ManyToOne(() => User, user => user.tournaments)
    @JoinColumn({
        name: 'created_by_id',
        referencedColumnName: 'id'
    })
    createdBy: User;

    @OneToMany(() => Team, teams => teams.tournament)
    teams: Team[];

    @OneToMany(() => Bets, bets => bets.tournament)
    bets: Bets[];

    @OneToMany(() => Matches, matches => matches.tournament)
    matches: Matches[];

    @Column('float', {
        nullable: true,
        transformer: {
            to: value => {
                return parseFloat(value) || 0;
            },
            from: value => {
                return parseFloat(value) || 0;
            }
        },
        default: 0.0001,
    })
    bet: number;

    @Column('int', {
        name: 'count_teams',
        default: 2
    })
    countTeams: number;

    @Column('enum', {
        enum: TournamentTypeEnum,
        nullable: false,
        default: TournamentTypeEnum.ONLY_ONE
    })
    type: TournamentTypeEnum;
}
