import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, } from 'typeorm';
import { Tournaments } from "./tournaments";
import { Team } from "./team";
import { MatchesStatusEnum } from "./enums/matches.status.enum";


@Entity('matches', {
    schema: 'chatex',
})

export class Matches extends BaseEntity {

    @PrimaryGeneratedColumn() id: number;

    @Column('timestamp without time zone', {
        nullable: true,
    })
    date: Date;

    @ManyToOne(() => Tournaments, tournament => tournament.matches)
    @JoinColumn({
        name: 'tournament_id',
        referencedColumnName: 'id'
    })
    tournament: Tournaments;

    @ManyToOne(() => Team)
    @JoinColumn({
        name: 'team_left_id',
        referencedColumnName: 'id'
    })
    teamLeft: Team;

    @ManyToOne(() => Team)
    @JoinColumn({
        name: 'team_right_id',
        referencedColumnName: 'id'
    })
    teamRight: Team;

    @ManyToOne(() => Team)
    @JoinColumn({
        name: 'winner_id',
        referencedColumnName: 'id'
    })
    winner: Team;

    @Column('int', {
        name: 'step',
        nullable: false,
        default: 2
    })
    step: number;

    @Column('enum', {
        enum: MatchesStatusEnum,
        nullable: false,
        default: MatchesStatusEnum.NEW
    })
    status: MatchesStatusEnum;
}
