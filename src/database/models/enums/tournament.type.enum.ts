export enum TournamentTypeEnum {
    ONLY_ONE = 'ONLY_ONE',
    TOP_THREE_60_30_10 = 'TOP_THREE_60_30_10',
    TOP_THREE_50_30_20 = 'TOP_THREE_50_30_20',
    ROUND_ROBIN = 'ROUND_ROBIN',
    MAX_RATE = "MAX_RATE"
}
