export enum MatchesStatusEnum {
    NEW = 'NEW',
    IN_PROCESS = 'IN_PROCESS',
    COMPLETE = 'COMPLETE',
    CANCELED = 'CANCELED'
}
