import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user';

@Entity('access_token', {
    schema: 'chatex'
})
export class Token extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column('text', {
        name: 'refresh_token',
        nullable: true
    })
    refresh: string;

    @Column('text', {
        name: 'token',
        nullable: true
    })
    token: string;

    @Column('int', {
        nullable: true,
        select: false
    })
    expires: number;

    @ManyToOne(() => User, user => user.tokens)
    @JoinColumn({
        name: 'user_id',
        referencedColumnName: 'id'
    })
    user: User;

}
