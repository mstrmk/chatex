import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, } from 'typeorm';
import { User } from "./user";
import { Tournaments } from "./tournaments";


@Entity('bets', {
    schema: 'chatex',
})

export class Bets extends BaseEntity {

    @PrimaryGeneratedColumn() id: number;

    @Column('timestamp without time zone', {
        nullable: true,
        default: () => '(now() at time zone \'Europe/Moscow\')',
    })
    created: Date;

    @ManyToOne(() => User)
    @JoinColumn({
        name: 'user_id',
        referencedColumnName: 'id'
    })
    user: User;

    @ManyToOne(() => Tournaments, tournament => tournament.bets)
    @JoinColumn({
        name: 'tournament_id',
        referencedColumnName: 'id'
    })
    tournament: Tournaments;

    @Column('float', {
        nullable: true,
        transformer: {
            to: value => {
                return parseFloat(value) || 0;
            },
            from: value => {
                return parseFloat(value) || 0;
            }
        },
        default: 0.0001,
    })
    amount: number;

    @Column('character varying', {
        name: 'status'
    })
    status: string;
}
