import 'reflect-metadata';
import { RegCommandService } from "@services/reg.command.service";

require('source-map-support').install();

const run = async () => {
    const init = new RegCommandService();
    await init.registration();
};

run().then();



