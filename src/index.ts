import 'reflect-metadata';

import { PostgreSQL } from './database/database';
import { logger } from '@config/logger';
import { ChatexService } from '@services/chatex.service';
import { RegCommandService } from '@services/reg.command.service';
import { UserService } from '@services/user.service';
import { BotService } from '@services/bot.service';

import TEXTS from '@config/texts';
import { User } from './database/models/user';
import { TournamentService } from '@services/tournament.service';
import { TeamService } from '@services/team.service';

import { Tournaments } from './database/models/tournaments';

const config = require('../config.json');
const { Client, Intents } = require('discord.js');
const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.DIRECT_MESSAGES, Intents.FLAGS.GUILD_MESSAGES] });

require('source-map-support').install();

const botService = new BotService();
const userService = new UserService();
const chatexService = new ChatexService();

// (new ChatexService()).auth('283721113475743750', 'mstrmk');
// (new ChatexService()).getToken(4);

const startMessageInfo = {
    content: TEXTS.first,
    components: [
        botService.makeRow().addComponents(
            botService.makeButton('how', 'Как это работает?'),
            botService.makeButton('join', 'Хочу участвовать'),
            botService.makeButton('createChamp', 'Хочу создать турнир'),
            botService.makeButton('watchChamps', 'Посмотреть турниры')
        )
    ],
    state: 'start'
}

const run = async () => {
    client.once('ready', async () => {
        console.log(`Logged in as ${client.user.tag}!`);
    });

    await PostgreSQL.createConnection();

    client.on('interactionCreate', async interaction => {
        if (!interaction.isCommand() && !interaction.isButton() && !interaction.isSelectMenu()) {
            return;
        }

        const userService = new UserService();
        const user = await userService.get(interaction.user);

        console.log(user);

        if (interaction.isCommand()) {
            switch (interaction.commandName) {
                case 'ping':
                    await interaction.reply({ content: 'Pong!', ephemeral: true });
                    break;
                case 'start':
                    await interaction.user.send({
                        content: startMessageInfo.content,
                        components: startMessageInfo.components
                    });
                    
                    await interaction.reply({ content: 'Мы отправили Вам личное сообщение\r\nЕсли оно не пришло, проверьте настройки!', ephemeral: true }); 
                    user.status = 'start';
                    user.save();
                break;
            }
        } else if (interaction.isButton()) {
            let content = null;
            let components = [];
            let newStatus = null;
            let replace = true;
            let newState = user.state;

            const buttonId = interaction.customId;

            switch (user.status) {
                case 'start':
                    switch (buttonId) {
                        case 'join':
                            content = TEXTS.joinChampRegCommand;
                            components = [
                                botService.makeRow().addComponents(
                                    botService.makeButton('needTeam', 'У меня неполный состав команды'),
                                )
                            ];
                            newStatus = 'joinChampRegCommand';
                        break;
                    }
                break;
                case 'joinChampRegCommand':
                    switch (buttonId) {
                        case 'needTeam':
                            content = startMessageInfo.content;
                            components = startMessageInfo.components;
                            newStatus = startMessageInfo.state;
                        break;
                    }
                break;
                case 'confirmMembersTeam':
                    switch (buttonId) {
                        case 'yes':
                            content = 'Отлично\r\nКак называется ваша команда?';
                            newStatus = 'team.name';
                        break;
                        case 'no':
                            content = TEXTS.joinChampRegCommand;
                            components = [
                                botService.makeRow().addComponents(
                                    botService.makeButton('needTeam', 'У меня неполный состав команды'),
                                )
                            ];
                            newStatus = 'joinChampRegCommand';
                        break;
                    }
                break;
                case 'buyTicket':
                    const tournamentService = new TournamentService();
                    switch (buttonId) {
                        case 'buy':
                            const champ = await Tournaments.findOne(user.state.champId);
                            content = 'Сумма к оплате: ' + champ.bet + ' BTC' + '\r\nВведите PIN-код Chatex для оплаты';
                            newStatus = 'buyTicket.pin';
                        break;
                        case 'back':
                            const champs = await tournamentService.find();
                            
                            let items = [];
                            champs.forEach(champ => {
                                items.push({
                                    label: champ.name,
                                    value: '' + champ.id
                                });
                            });

                            content = 'Выберите турнир';
                            components = [
                                botService.makeRow().addComponents(
                                    botService.makeSelectMenu('champ', items)
                                )
                            ];
                            newStatus = 'selectChamp';
                        break;
                    }
                break;
            }

            if (content) {
                if (!replace) {
                    await interaction.user.send({ content, components });
                } else {
                    await interaction.update({ content, components });
                }
            }

            if (newStatus) {
                user.status = newStatus;
            }
            if (newState != user.state) {
                user.state = newState;
            }
            if (newStatus || newState != user.state) {
                user.save();
            }
        }
        else if (interaction.isSelectMenu()) {
            let content = null;
            let components = [];
            let newStatus = null;
            let replace = true;
            let newState = user.state;

            const selectId = interaction.customId;

            switch (user.status) {
                case 'selectChamp':
                    const champId = Number(interaction.values[0]);
                    newState.champId = champId;

                    const champ = await Tournaments.findOne(champId);
                    content = 'Супер! Ты выбрал турнир: ' + champ.name + '\r\n\r\nОплати взнос со своего кошелька Chartex, и мы зарегистрируем вашу команду. Я дам тебе код-билеты, отправь их своей команде, чтобы твои игроки смогли подтвердить участие через бот.';
                    components = [
                        botService.makeRow().addComponents(
                            botService.makeButton('buy', 'Оплатить', 'SUCCESS'),
                            botService.makeButton('back', 'Выбрать другой турнир')
                        )
                    ];
                    newStatus = 'buyTicket';
                break;
            }

            if (content) {
                if (!replace) {
                    await interaction.user.send({ content, components });
                } else {
                    await interaction.update({ content, components });
                }
            }

            if (newStatus) {
                user.status = newStatus;
            }
            if (newState != user.state) {
                user.state = newState;
            }
            if (newStatus || newState != user.state) {
                user.save();
            }
        }

    });

    client.on('messageCreate', async message => {
        if (message.author.bot) {
            return;
        }

        const user = await userService.get(message.author);

        let content = null;
        let components = [];
        let newStatus = null;
        let newState = user.state;

        switch (user.status) {
            case 'joinChampRegCommand':
                const ids = message.content.replaceAll(' ', '').split(',');
                if (ids.length !== 5) {
                    content = 'Неверный формат';
                }
                else {
                    newState = {team_ids: ids};
                    content = 'Давай сверятся, это твои ребята?\r\nМаксим Калинушкин\r\nВячеслав Троицкий\r\nЕго величество - Сковорода\r\nОригинальный Ефим\r\nПлавательный';
                    newStatus = 'confirmMembersTeam';
                    components = [
                        botService.makeRow().addComponents(
                            botService.makeButton('yes', 'Да, все верно', 'SUCCESS'),
                            botService.makeButton('no', 'Нет, я опечатался', 'DANGER')
                        )
                    ];
                }
            break;
            case 'team.name':
                const tournamentService = new TournamentService();
                const champs = await tournamentService.find();
                
                let items = [];
                champs.forEach(champ => {
                    items.push({
                        label: champ.name,
                        value: '' + champ.id
                    });
                });
                content = 'Выберите турнир';
                components = [
                    botService.makeRow().addComponents(
                        botService.makeSelectMenu('champ', items)
                    )
                ];
                newState.teamName = message.content;
                newStatus = 'selectChamp';
            break;
            case 'buyTicket.pin':
                const pin = message.content;
                const champ = await Tournaments.findOne(user.state.champId);
                const result = await chatexService.transfer(user.id, pin, champ);

                if (result) {
                    const teamService = new TeamService();
                    await teamService.create(user, champ, user.state.teamName, user.state.team_ids);
                }
                else {
                    content = 'Ошибка. Неверный пин или не хватает средств';
                }
            break;
        }


        if (content) {
            await message.author.send({ content, components });
        }

        if (newStatus) {
            user.status = newStatus;
        }
        if (newState != user.state) {
            user.state = newState;
        }
        if (newStatus || newState != user.state) {
            user.save();
        }
    });

    await client.login(config.token).catch(console.error);

    console.log('LOADED');


    // const init = new RegCommandService();

    // await init.registration();

};

run().then();



